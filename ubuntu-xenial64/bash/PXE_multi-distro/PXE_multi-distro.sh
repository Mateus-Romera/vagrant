#!/usr/bin/env bash

# Its necessary to change ip address on the files below with the ip address of the server (ip addr)
# Clonezilla.menu (static ip address)
# dnsmasq.conf (network ip address)
# exports: you can use (*) to all ips or use static ip address
# Ubuntu.menu: (static ip address)

# Use boot_repository files for quicker deployment.
# Otherwise, you will have to download all the files again (.iso, netboot, etc).
# Local boot_repository = /vagrant/boot_repository
repo_enable=true
repo_path=/vagrant/files_repository
# Netboot, iso and other files URL
ubuntu_1604_server_netboot_url="http://archive.ubuntu.com/ubuntu/dists/xenial-updates/main/installer-amd64/current/images/netboot/netboot.tar.gz"
ubuntu_1604_desktop_iso_url="http://releases.ubuntu.com/xenial/ubuntu-16.04.5-desktop-amd64.iso"
clonezilla_iso_url="https://osdn.net/projects/clonezilla/downloads/70505/clonezilla-live-2.6.0-37-amd64.iso"


# Turn off front end prompts during packages installation
export DEBIAN_FRONTEND=noninteractive

# Update the package lists for upgrades
apt-get update

# Install locales pt_BR package
apt-get install -y language-pack-pt

# Install dnsmasq, pxelinux, syslinux and nfs-kernel-server packages
apt-get install -y dnsmasq pxelinux syslinux nfs-kernel-server

# Create default folder for downloads
mkdir /vagrant/downloads

# Create default folder for tftpboot directory
mkdir -p /var/lib/tftpboot


# dnsmasq
# Replace default dnsmasq.conf file with an already configured dnsmasq.conf file
rm /etc/dnsmasq.conf
cp /vagrant/config_files/dnsmasq.conf /etc/
# Replace default dnsmasq file with an already configured dnsmasq file
rm /etc/default/dnsmasq
cp /vagrant/config_files/dnsmasq /etc/default/

# Restart dnsmasq service
service dnsmasq restart


# syslinux
# Create pxelinux.cfg default directory
mkdir /var/lib/tftpboot/pxelinux.cfg

# Copy all necessary files to pxe-server work correctly
cp /usr/lib/PXELINUX/pxelinux.0 /var/lib/tftpboot/
cp /usr/lib/syslinux/modules/bios/vesamenu.c32 /var/lib/tftpboot/
cp /usr/lib/syslinux/modules/bios/ldlinux.c32 /var/lib/tftpboot/
cp /usr/lib/syslinux/modules/bios/libcom32.c32 /var/lib/tftpboot/
cp /usr/lib/syslinux/modules/bios/libutil.c32 /var/lib/tftpboot/
# Copy already configured default file for pxelinux.cfg
cp /vagrant/config_files/default /var/lib/tftpboot/pxelinux.cfg/
# Copy already configured default file for pxe.conf
cp /vagrant/config_files/pxe.conf /var/lib/tftpboot/pxelinux.cfg/


# nfs-kernel-server
# Create install nfs folder server
mkdir /srv/install

# Replace default exports file with and already configured one
rm /etc/exports
cp /vagrant/config_files/exports /etc/

# Restart nfs-kernel-server service
service nfs-kernel-server restart


# Ubuntu
# Ubuntu 16.04-5 LTS Desktop
# Create Ubuntu Desktop folder on tftpboot and nfs locations
mkdir -p /var/lib/tftpboot/Ubuntu/16.04/desktop/amd64
mkdir -p /srv/install/Ubuntu/16.04/desktop/amd64

# Download Ubuntu 16.04-5 LTS Desktop iso
# wget used option:
# --connect-timetout=20: wait 20 seconds to timetout
# --tries 3: try downloading 3 times before giving up
# --no-clobber (-nc): skip download if file already exists
# --quiet: do not show progress or verbose while downlading
# --directory-prefix (-P): set directory prefix for download file
wget --connect-timeout=20 --tries 3 --no-clobber --quiet --directory-prefix /vagrant/downloads/ $ubuntu_1604_desktop_iso_url
# Create folder for iso mounting
mkdir /mnt/loop
# Mount iso
mount -o loop -t iso9660 /vagrant/downloads/ubuntu-16.04.5-desktop-amd64.iso /mnt/loop
# Copy relevant files to tftpboot and nfs locations
cp /mnt/loop/casper/vmlinuz /var/lib/tftpboot/Ubuntu/16.04/desktop/amd64
cp /mnt/loop/casper/initrd /var/lib/tftpboot/Ubuntu/16.04/desktop/amd64
cp -R /mnt/loop/* /srv/install/Ubuntu/16.04/desktop/amd64
cp -R /mnt/loop/.disk /srv/install/Ubuntu/16.04/desktop/amd64
# Unmount iso
umount /mnt/loop

# Ubuntu 16.04 Server
# Create Ubuntu Server folder on tftpboot location
mkdir -p /var/lib/tftpboot/Ubuntu/16.04/server/amd64

# Download Ubuntu Server netboot
# wget used option:
# --connect-timetout=20: wait 20 seconds to timetout
# --tries 3: try downloading 3 times before giving up
# --no-clobber (-nc): skip download if file already exists
# --quiet: do not show progress or verbose while downlading
# --directory-prefix (-P): set directory prefix for download file
wget --connect-timeout=20 --tries 3 --no-clobber --quiet --directory-prefix /vagrant/downloads/ $ubuntu_1604_server_netboot_url
# Create folder to unzip netboot files
mkdir /vagrant/downloads/16.04_amd64-netboot/
# Extract netboot files
tar -xzf /vagrant/downloads/netboot.tar.gz -C /vagrant/downloads/16.04_amd64-netboot/
# Copy relevant files to tftpboot location
cp /vagrant/downloads/16.04_amd64-netboot/ubuntu-installer/amd64/linux /var/lib/tftpboot/Ubuntu/16.04/server/amd64/
cp /vagrant/downloads/16.04_amd64-netboot/ubuntu-installer/amd64/initrd.gz /var/lib/tftpboot/Ubuntu/16.04/server/amd64/

# Copy already configured default file for Ubuntu.menu (Desktop and Server)
cp /vagrant/config_files/Ubuntu.menu /var/lib/tftpboot/Ubuntu/


# Clonezilla
# Create Clonezilla folder on tftpboot location
mkdir -p /var/lib/tftpboot/Clonezilla/2.6.0-37/

# Download Clonezilla Live iso
# wget used option:
# --connect-timetout=20: wait 20 seconds to timetout
# --tries 3: try downloading 3 times before giving up
# --no-clobber (-nc): skip download if file already exists
# --quiet: do not show progress or verbose while downlading
# --directory-prefix (-P): set directory prefix for download file
wget --connect-timeout=20 --tries 3 --no-clobber --quiet --directory-prefix /vagrant/downloads/ $clonezilla_iso_url
# Mount iso
mount -o loop -t iso9660  /vagrant/downloads/clonezilla-live-2.6.0-37-amd64.iso /mnt/loop
# Copy relevant files to tftpboot and nfs locations
cp /mnt/loop/live/vmlinuz /var/lib/tftpboot/Clonezilla/2.6.0-37/
cp /mnt/loop/live/initrd.img /var/lib/tftpboot/Clonezilla/2.6.0-37/
cp /mnt/loop/live/filesystem.squashfs /var/lib/tftpboot/Clonezilla/2.6.0-37/
# Unmount iso
umount /mnt/loop

# Copy already configured default file for Clonezilla.menu
cp /vagrant/config_files/Clonezilla.menu /var/lib/tftpboot/Clonezilla/


# memtest86+
# maybe later addition
