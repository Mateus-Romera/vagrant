#!/usr/bin/env bash

# Turn off front end prompts during installation.
export DEBIAN_FRONTEND=noninteractive

# Update the package lists for upgrades
apt-get update

# Install locales pt_BR
apt-get install -yq language-pack-pt

# Install tshark and pre-enable 'non root users'
echo "wireshark-common wireshark-common/install-setuid boolean true" | debconf-set-selections
apt-get install -y tshark
# Add vagrant user to wireshark group
usermod -a -G wireshark vagrant

# Install ethtool package
apt-get install -y ethtool

# Install apache2 -y for auto-yes
apt-get install -y apache2
# Insert domain host at the ending of apache2 config file
echo "ServerName localhost" >> /etc/apache2/apache2.conf
# Open ports 80/443 in firewall (although firewall is not enable on vagrant, is a great exercise to remember to mess with firewall after nginx or apache2 instalation)
ufw allow in "Apache Full"
# Remove current 000-default.conf file to replace with pre-configured file
rm /etc/apache2/sites-available/000-default.conf
# Create new symbolic link for pre-configured 000-default.conf file
cp /vagrant/config_files/000-default.conf /etc/apache2/sites-available/000-default.conf
#ln -s /etc/apache2/sites-available/000-default.conf /vagrant/000-default_os-link.conf

# Create html folder for web development
mkdir /vagrant/html
# Map /var/www to shared /vagrant/html directory
if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

# Install Node.js and NPM
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt-get install -y nodejs

# Install MySQL, prepopulate password to 'root'
echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
apt-get install -y mysql-server mysql-client

# Update the repositories for PHP7 (not required after Ubuntu 16.10)
LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php # Had to add LC_ALL UTF-8 due to the name of this repository - broken ubuntu :(
apt-get update

# Install PHP, MySQL helper packagemysql and mbstring for phpmyadmin
apt-get install -yq php7.3-fpm php7.3-mysql php7.3-mbstring
# Remove current php.ini file to replace with pre-configured file
rm /etc/php/7.3/fpm/php.ini
# Create new symbolic link for pre-configured php.ini file
cp /vagrant/config_files/php.ini /etc/php/7.3/fpm/php.ini
#ln -s /etc/php/7.3/fpm/php.ini /vagrant/php_os-link.ini

# Enable PHP 7.3 FPM in Apache2
a2enmod proxy_fcgi setenvif
a2enconf php7.3-fpm
# Enable mod_rewrite in Apache2
a2enmod rewrite

# Install phpmyadmin
APP_PASS="root"
ROOT_PASS="root"
echo "phpmyadmin phpmyadmin/app-password-confirm password $APP_PASS" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $ROOT_PASS" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect apache2" | debconf-set-selections
apt-get install -yq phpmyadmin
# Create phpmyadmin symbolic link to html folder
ln -s /usr/share/phpmyadmin/ /vagrant/html/

# Restart apache2 and php services
service php7.3-fpm restart
service apache2 restart
