#!/usr/bin/env bash

# Turn off front end prompts during installation.
export DEBIAN_FRONTEND=noninteractive

# Update the package lists for upgrades
apt-get update

# Install locales pt_BR
apt-get install -y language-pack-pt

# Install tshark and pre-enable 'non root users'
echo "wireshark-common wireshark-common/install-setuid boolean true" | debconf-set-selections
apt-get install -y tshark
# Add vagrant user to wireshark group
usermod -a -G wireshark vagrant

# Install ethtool package
apt-get install -y ethtool

# Install nginx -y for auto-yes
apt-get install -y nginx
# Remove current nginx.conf to replace with pre-configured file
rm /etc/nginx/nginx.conf
# Create new simbolyc link for pre-configured nginx.conf file
cp /vagrant/config_files/nginx.conf /etc/nginx/nginx.conf
#ln -s /etc/nginx/nginx.conf /vagrant/nginx_os-link.conf
# Configure a new host file to localhost using a pre-configured file
cp /vagrant/config_files/nginx-localhost /etc/nginx/sites-available/nginx-localhost
#ln -s /etc/nginx/sites-available/nginx-localhost /vagrant/nginx-localhost_os-link
# Create new simbolyc link to new host localhost file
ln -s /etc/nginx/sites-available/nginx-localhost /etc/nginx/sites-enabled/nginx-localhost
# Disabling default nginx from sites-enabled
rm /etc/nginx/sites-enabled/default
# To test the nginx configuration file, use the command below
#sudo nginx -t

# Create html folder for web development
mkdir /vagrant/html
# Map /var/www to shared /vagrant/sites directory
if ! [ -L /var/www ]; then
    rm -rf /var/www
    ln -fs /vagrant /var/www
fi

# Install Node.js and NPM
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt-get install -y nodejs

# Install MySQL, prepopulate password to 'root'
echo "mysql-server mysql-server/root_password password root" | debconf-set-selections
echo "mysql-server mysql-server/root_password_again password root" | debconf-set-selections
apt-get install -y mysql-server mysql-client

# Update the repositories for PHP7 (not required after Ubuntu 16.10)
LC_ALL=C.UTF-8 add-apt-repository ppa:ondrej/php # Had to add LC_ALL UTF-8 due to the name of this repository - broken ubuntu :(
apt-get update

# Install PHP, MySQL helper packagemysql and mbstring for phpmyadmin
apt-get install -y php7.3-fpm php7.3-mysql php7.3-mbstring
# Remove current php.ini file to replace with pre-configured file
rm /etc/php/7.3/fpm/php.ini
# Create new symbolic link for pre-configured php.ini file
cp /vagrant/config_files/php.ini /etc/php/7.3/fpm/php.ini
#ln -s /etc/php/7.3/fpm/php.ini /vagrant/php_os-link.ini

# Install phpmyadmin
APP_PASS="root"
ROOT_PASS="root"
echo "phpmyadmin phpmyadmin/app-password-confirm password $APP_PASS" | debconf-set-selections
echo "phpmyadmin phpmyadmin/mysql/admin-pass password $ROOT_PASS" | debconf-set-selections
echo "phpmyadmin phpmyadmin/dbconfig-install boolean true" | debconf-set-selections
echo "phpmyadmin phpmyadmin/reconfigure-webserver multiselect " | debconf-set-selections
apt-get install -yq phpmyadmin
# Create phpmyadmin symbolic link to html folder
ln -s /usr/share/phpmyadmin/ /vagrant/html/

# Restart nginx and php services
service php7.3-fpm restart
nginx -s reload
