# REPOSITORY DESCRIPTION:

Vagrant boxes customized for software development and application/enviroment testing.

Also boxes for automated system deployment testing.

### Boxes softwares description:
- Ubuntu Xenial Xerus (16.04.5 LTS - 64 bit);
- Tshark;
- ethtool;
- LAMP stack (Apache2, MySQL 14.14 and PHP 7.3.1) or LEMP stack (Nginx, MySQL 14.14 and PHP 7.3.1);
- phpMyAdmin;
- Node.js 10 and NPM;
- Configuration files for packages above;

Current two boxes with minor differences.

### PXE Server Box description:
- dnsmasq (proxyDHCP);
- pxelinux;
- syslinux;
- nfs-kernel-server;
- tftpboot;
- Available bootable images/netboot:
    - Ubuntu 16.04-5 LTS Desktop (amd64);
    - Ubuntu 16.04 Server (amd64);
    - Clonezilla Live;
- Configuration files for packages above;

### To-Do:
- Install both Apache2 and Nginx on one single box;